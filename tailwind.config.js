const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      fontFamily: {
        mono: [
          'Nimbus Sans L',
          ...defaultTheme.fontFamily.mono
        ]
      }
    },
  },
  plugins: [],
}

