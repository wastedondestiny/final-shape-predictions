import { Router, RouterContext } from '@tsndr/cloudflare-worker-router'
import { D1Orm, DataTypes, Model } from 'd1-orm'

const router = new Router<Env>()

router.cors()
router.debug()

// ROUTES
router.get('/:membershipId', async ({ env, req }: RouterContext<Env, unknown, unknown>) => {
  const { leaderboard } = initialize(env)
  const leaderboardResults = await retryDatabase(
    () =>
      leaderboard.First({
        where: {
          membershipId: req.params.membershipId
        }
      }),
    2
  )

  return new Response(leaderboardResults?.predictions ?? '')
})

router.post('/', async ({ env, req }: RouterContext<Env, unknown, unknown>) => {
  return new Response('Forbidden', { status: 403 })

  if (req.headers.get('X-Auth-Key') !== env.WORKER_AUTH_KEY)
    return new Response('Forbidden', { status: 403 })
  if (!req.raw.body) return new Response('Forbidden', { status: 403 })

  const { leaderboard } = initialize(env)
  const body = (await req.json()) as { accessToken: string; predictions: string }

  if (!body) return new Response('Forbidden', { status: 403 })

  const result = (await fetch('https://www.bungie.net/Platform/User/GetCurrentBungieNetUser/', {
    headers: {
      authorization: `Bearer ${body.accessToken}`,
      'x-api-key': 'ab65a78a261b48339da0c6c367c397e4'
    }
  }).then((x) => x.json())) as { Response?: { uniqueName: string; membershipId: string } }

  if (!result.Response?.membershipId) return new Response('Forbidden', { status: 403 })

  await retryDatabase(
    () =>
      leaderboard.Upsert({
        data: {
          membershipId: result.Response!.membershipId,
          displayName: result.Response!.uniqueName,
          predictions: body.predictions,
          result: 0,
          rank: 0,
          updatedAt: new Date(Date.now()).toISOString(),
          createdAt: new Date(Date.now()).toISOString()
        },
        upsertOnlyUpdateData: {
          displayName: result.Response!.uniqueName,
          predictions: body.predictions,
          updatedAt: new Date(Date.now()).toISOString()
        },
        where: {
          membershipId: result.Response!.membershipId
        }
      }),
    2
  )

  return new Response('Success')
})

router.any('*', () => {
  return new Response('Not Found')
})

// PRIVATE METHODS
function initialize(env: Env) {
  const orm = new D1Orm(env.DB)
  const leaderboard = new Model(
    {
      D1Orm: orm,
      tableName: 'leaderboard',
      primaryKeys: 'membershipId'
    },
    {
      membershipId: { type: DataTypes.STRING, notNull: true },
      displayName: { type: DataTypes.STRING, notNull: true },
      predictions: { type: DataTypes.STRING, notNull: true },
      result: { type: DataTypes.INTEGER, notNull: true },
      rank: { type: DataTypes.INTEGER, notNull: true },
      createdAt: {
        type: DataTypes.STRING,
        notNull: true,
        defaultValue: () => new Date(Date.now()).toISOString()
      },
      updatedAt: {
        type: DataTypes.STRING,
        notNull: true,
        defaultValue: () => new Date(Date.now()).toISOString()
      }
    }
  )
  return { leaderboard }
}

async function retryDatabase<T>(action: () => Promise<T>, times: number) {
  let retries = 0

  while (retries < times) {
    try {
      return await action()
    } catch (error: Error | unknown) {
      if (++retries >= times) {
        throw error
      }

      await new Promise((resolve) => setTimeout(resolve, 2 ** retries * 1000))
    }
  }

  return null as T
}

// EXPORT
export default {
  async fetch(request: Request, env: Env, ctx: ExecutionContext) {
    return router.handle(request, env, ctx)
  }
}
