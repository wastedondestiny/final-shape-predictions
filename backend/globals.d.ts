declare interface Env {
	DB: D1Database,
  WORKER_AUTH_KEY: string,
  WORKER_RESEND_KEY: string
}
