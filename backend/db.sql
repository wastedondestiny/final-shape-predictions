CREATE TABLE IF NOT EXISTS `leaderboard` (
  `membershipId` varchar NOT NULL,
  `displayName` varchar NOT NULL,
  `predictions` varchar NOT NULL,
  `result` INTEGER NOT NULL,
  `rank` INTEGER NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`membershipId`)
);
